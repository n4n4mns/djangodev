​
Django user auth. Forms
If auth needs differ from defaults, see ‘extension and customisation of user authentication’.
Django provides authentication and authorisation together. Requires django.contrib.auth I’m INSTALLED_APPS
USER OBJECT
Super users and admin accounts are derived from user objects
At terminal …
$ python manage.py createsuperuser --username=joe --email=joe@example.com
Main class
from django.contrib.auth.models import User
Default user creation
Newuser = User.objects.create_user(name, email, pwd)
Authenticating users:
from django.contrib.auth import authenticate
Provides low level access. See instead login_required()
Permissions
Access to forms and model objects can be restricted to users based on add, change and delete permissions. Perms can be set per object type and customised per instance using the ModelAdmin class.
User objects have two many to many fields, user.groups and user.user_permissions. Both come with set, add, remove and clear methods to assign user groups and permissions respectively.
To test a user has permission for the model ‘bar’ in the app ‘foo’ use:
User.has_perm(‘foo.add_bar’).
GROUPS
Django.contrib.auth.models.Group models allow user categorisation so a user added to a group has all the permissions. Useful for custom, functionality: create a members only web page or members only emails.
CREATING PERMISSIONS
Define custom permissions in a models Meta class, or create them directly? For example:
From django.contrib.auth.models import Permission
permission = Permission.objects.create( code name=“can_publish”, name=“Can publish posts”, content_type=content_type). These permissions can be added via User.user_permissions or Group.permissions.
Segue: CONTENT TYPES
The django contenttypes framework tracks all the models installed in your django powered project and provides a high-level, generic interface for working with your models. In the above example, content_type is an instance of django.contrib.contenttypes.models.ContentType.objects.get_for_model(MyModel), where my model is the model you wish to grant the custom permission to. ContentType instances
PERMISSION CACHING
Model backend caches permissions on the user object after the first time they need to be fetched for a permission check. Typically in the request response cycle, permissions aren’t checked immediately after being added. To do so in one cycle, re-fetch the user from the database using django.shortcuts.get_object_or_404(User, pk=user_id) before checking with user.has_perm.
Segue: web caching is the process of temporarily storing content from previous requests. It is part of the core content delivery strategy in HTTP and serves to make subsequent requests faster
AUTHENTICATION + REQUESTS
Django uses sessions and middleware to hook the authentication system into request objects. Every request which represents the current user will have a requests.user attribute, which is set to an instance of User if they are logged in or AnonymousUser if not . You can use request.user.is_authenticated to tell them apart.
LOG A USER IN
We attach authenticated users to the current ‘session’ from a view using django.contrib.auth.login(request, user, backend=None). login() takes a HttpRequest object, and a User object (retrieved using authenticate()) and saves the users ID to the session using Djangos session framework.
LOG A USER OUT
Use django.contrib.auth.logout() within your logout view then redirect. Logout a takes HttpRequest and outputs no response. It cleans the session data meaning the same browser can’t be used to access a previous users session data.
SELECTING THE AUTHENTICATION BACKEND
The users ID and backend used for authentication are saved in the users session upon login. This allows the auth backend, saved in a session, to fetch the user details on a future request. Backend can be passed as the optional arg to login(), but isn’t necessary as authenticate() sets a user.backend attribute on the object it returns. Optionally the backend can be set in AUTHENTICATION_BACKENDS. In all cases the attribute should be a dotted import path string, not the actual backend class.
LIMIT ACCESS TO LOGGED IN USERS
Raw way: check requests.user.is_authenticated and of true, redirect .
Easier way: @login_required decorator (from django.contrib.auth.decorators). Can take arguments for redirect_field_name and login_url. Similarly, the class LoginRequiredMixin can be inherited by a class based view (must be left-most) and parameters overridden. Note: neither the login required decorator or mixin check the is_active flag, but the AUTHENTICATION_BACKENDS rejects inactive users.
Similarly the @user_passes_test and UserPassesTestMixin can be used to limit access based on a test function which returns true or false.
Finally @permission_required and class PermissionRequiredMixin check wether user accessing view has given permissions.
AccessMixin can redirect a user of a class based view to the login page or return 403 forbidden response.
AUTHENTICATION VIEWS
Django provides several views for handling login, logout and password management. They also make use of the *stock auth forms*. Implement by
url(‘^’, include(‘django.contrib.auth.urls))
auth_views.PasswordChangeView.as_view
Former includes all, the latter uses specific views.
BUILT IN AUTH FORMS
django.contrib.auth.forms: allow you to use the auth forms in your own views. See UserCreationForm(ModelForm) for creating registration pages.
SEGUE - DJANGO FORMS (Mozilla tutorial)
HTML forms are designed to take user input from the website and pass this data to the server for processing - more specifically this data is passed to another url on the website (and in Django is accessed via the request variable).
<form action=“URL/loc.html” method=“POST”> <!— declare the form tag with the action url to send data to and the HTTP verb or method to send it —>
<label for=“field1”>Enter data:</label> <!— label to display text for the info field, the for attribute must match an id —>
<input id=“field1” type=“text” value=“default text” name=“myfield”> <!— input fields contain the data a user enters. Type determines the widget that’s shown. Name allows the data to be referenced after form submission and value is default text. Id allows it to be referenced by CSS and label—>
<button type=“submit”> Button message </button> <!— all forms require an button of type SUBMIT, which sends the form data to the action URL on click—>
</form>
With that background we are ready to discuss forms in Django.
Working with forms can be complicated! Developers need to write HTML for the form, validate data, repost the form with error messages if invalid fields exist, handle the data when it has been submitted and respond to the user to indicate success.
In Django, create an object that inherits from django.forms.Form Form object. The forms module provides classes for all the form data required in html; fields, layout, display widgets, labels, initial values, valid values, and the error messages associated with invalid fields. It also has methods for rendering templates and for getting the value of any element.
The syntax for declaring a Form object is similar to that for declaring a model. Within the class we create variables for form fields - these are instances of form classes (often with equivalent model classes) that are pre formatted to take certain data types eg forms.DateField, forms.CharField.
Simplest way to validate a field is to override the method clean_<field name>(self) for the field you want to check.
-From docs
“AForminstance has an is_valid() method, which runs validation routines for all its fields. When this method is called, if all fields contain valid data, it will: • return True & place the form's data in its cleaned_data attribute. Rendered form does not include <form> tags or a submit button.”
USING THE FORM
In the form handling process, a view renders the default form when it’s first called and then either re-renders with error messages if invalid, or process and redirect if valid. The view needs to know if the form is being called for the first time (render default)or a subsequent time (validate data).
The most common pattern is test for POST request to validate form or GET request to render default .
E.g
def mymodel(request, pk):
# get the model required to populate the form
modelfield = get_object_or_404(ModelInstance, pk = pk)
# here we pass form data in request to a form class instance and store it. This process is called ‘binding’ and allows us to validate the form.
if request.method == ‘POST’:
form = myform(request.POST)
# once validated, form data can be stored to the database
If form.is_valid(): # .is_valid() runs all validation code on all fields, including the generic validation that comes with form field objects and our self defined cleaned_<field_name> object.
modelfield = form.cleaned_data[‘myfield’]
modelfield.save()
Return HttpResponseRedirect(reverse(‘done-page’))
# the reverse method generates a url from a url conf name and a set of arguments
#if this is a GET or any other method, render the default page.
Else:
form = myform()
Return render(request, ‘users/my_template.html, {‘form’ : form})
# The form object is passed to the render function as part of the context . This allows the template to call the specific form objects as required
# restrict access to the view using the permission required function decorator
In the template referenced in the view, the form section may look like this:
#note, an empty action means the form url is passed back to the current page
<form action=“” method=“POST”>
{% csrf_token %}
<table>
{{ form }}
</table>
<input type=“submit” value=“Submit”>
</form>
The default {{ form }} variable call in the template calls form.as_table, hence the need for the enclosing table tags. You can also render each field as a list item (using {{form.as_ul}} ) or as a paragraph (using {{form.as_p}}). Fields can also be rendered manually as methods of the form class.
———————
MODEL FORMS
creating forms is flexible as you can map the form page to any models you like.
If your form fields map to the fields of a single model, then recreating model definitions in your form is redundant : use django.forms.ModelForm
You create a form class that inherits from ModelForm. Within this you create a class Meta:
model = model instance
fields = [ ‘modelfield1’, ‘modelfield2’, ....]
That’s it! You can use ‘_all_’ to include all fields from the model. The form uses defaults but these can all be overridden using the attribute (eg form_field = dict() - model_field:value pairs for the corresponding form input).
To add validation is the same as a normal form. Def clean_<fieldname>, being aware the field name is now that of the model field not the form field.
These clean functions should always raise ValidationError.
————————
GENERIC EDITING VIEWS
Django abstracts the boilerplate of form algorithm by supplying generic editing views: create, edit and delete views based on models. They handle the view behaviour and automatically create the form class (a ModelForm).
<note, there is also a FormView. you still need to create your Form, but you don't have to implement all of the standard form-handling pattern. Instead, you just have to provide an implementation of the function that will be called once the submitted is known to be be valid.>
—————————
User Auth 2
the User and Group models are provided for restricting access to pages. Generally we can test for user.is_authenticated in templates or supply the @login_required decorator to views (there is also a mixin for this).
Permissions are associated with models and define the operations a user can perform (add, change and delete). They are created in the models Meta class and tested for in templates using {{perms}}, or on views using @permission_required() decorator. Once assigned to users, they will have access to model editing views/page functions.
<saved here to google drive on 051217>
Django shortcut functions
Found in Django.shortcuts these helper functions span multiple levels of MVC.
render() - combine a template with a given context dictionary and return HttpResponse. Requires args ‘request’ and ‘template’. Note: content_type defines the The MIME type to use for the resulting document. Defaults to the value of the DEFAULT_CONTENT_TYPE setting.
Redirect() -
Django middleware
Django sessions

​
DJANGO CLASS BASED VIEWS
*from ‘USING DJANGO’
A view is simply a callable that takes a request and returns a response. Django offers class based views that allow code to be structured and reused through inheritance, generic views and mixins. Class-based views also offer organisation of code to specific HTTP methods.
All class-based views inherit from the View class. Django provides several generic class views for typical operations such as ListView for showing model data and FormView for displaying forms.
Using class based views (CBVs)
All CBVs inherit from django.views.View. We can define the procedure for a HTTP GET request by simply defining a function in the class with the HTTP verb:
def get(self,request)
In the URLCONF, Django expects a view to be callable. We achieve this with classes using the .as_view() method. This returns a function which is responsible for creating a class instance and calling its dispatch() method. Dispatch determines the type of HTTP request and calls the matching method in the class, or raises HttpResponseNotAllowed if not.
At the point of the dispatch call, the class method is essentially a function based view, so http shortcuts and TemplateResponse objects are valid in CBVs.
Class attributes (variables defined in the class scope) can be overridden between CBVs the standard pythonic way.
Using Mixins
Mixins are a form of multiple inheritance whereby the behaviour of a CBV is altered by the inheritance of Mixin classes. They allow excellent code reuse across multiple classes, but make it difficult to read a child class and know exactly what it is doing.
NB: You can only inherit from one generic view - only one parent class may inherit from View, the rest should be mixins.
Decorating Class based views
We can decorate CBVs with django permission functions using the URLCONF or @method_decorator in the class definition. See docs for details.
Built in generic class based views
Generic views abstract common view development patterns, making it easier to perform common tasks. Django provides generic views for displaying list and detail pages for object, presenting date based archive pages and views that allow users to create, update and delete objects. To extend the functionality of generic views, we subclass them and override the methods. Here, this tutorial describes useful common tasks for CBVs. See the generic views reference for all possible views.
Extending generic views
After subclassing, we simply change the model attribute to a user defined model.
The template can be given through the url conf or by adding a template_view attribute to the view.
The template is rendered against a context called object_list by default, which contains the objects from the model passed to the view. We can change the name of this context by adding the attribute context_object_name. We can also add extra contexts and models by overriding get_context_data in our class;
#import models
class MyDetail(DetailView):
model = myfirstmodel
def get_context_data(self, **kwargs):
#call base implementation first to get a context
context = super(MyDetail,self).get_context_data(**kwargs)
# Add in a Queryset of second models
context[‘model2’] = secondmodel.objects.all()
Note that we added a queryset in the second model. The model override is actually shorthand for myfirstmodel.objects.all(), which returns a model queryset. Using querysets allows us to filter the list of objects more specifically. For single use in the class, simply provide a queryset attribute instead of a model one.
Dynamic filtering
Performing extra work
Within the class view, we define a ‘Meta’ class to override the defaults. Here we can set a new model, context, template and more.


​
Django MOOC
Django 1.1
Describes django installation
NB:
Running a django app in python virtual environments allows you to tea st new features and dependency updates, without breaking the current version. It is good practice to supply and document specific versions.
Django 1.2
Describes django app start, app directory structure and run server command
NB:
A ‘migration’ is the reversible transfer of a database from one format to another.
Django 1.3
Django applications are pluggable, allowing reuse in different web programs. Describes the mapping of views to templates.
—-Django 1.4 Challenge
—-Django 1.5 Challenge solution
Django 1.6
Describes how apps can have their own urls.py that links to the project urls.py using include().
Django 1.7
The TEMPLATES variable in the project’s settings.py informs Django of the location of template html files. Describes how context dictionary is passed to template.
NB; Django handles portability between PCs - best practice is to have a subfolder within templates directory with the name of the app.
—-1.8 Templates challenge
—-1.9 Templates solution
Django 1.10 static files
Create a ‘static’ directory in the project and add it to the STATIC_URL in the settings.py file.
The syntax in Django Templates is:
{% load static %} #after !Doctype
<img src={%static “images/pic.jpg” %} />
It is also best practice to have static folders within each app. We are able to define a list of static directories in the settings.py (see docs for detail).
Static includes CSS and JAVASCRIPT.
Django 2.1
Recaps the process of views, urls, and templates.
Note: We often need access to URLs either for embedding content in templates or controlling navigation server side. Hard coding URLs throughout our app is out of the question. The django URL mapper performs URL reversing where, given the identification of a view (by the name attribute in path() in urlpatterns) and arguments passed to it, obtain the associated URL.
Django provides tools for performing URL reversing that match the different layers where URLs are needed:
• In templates: Using the url template tag.
• In Python code: Using the reverse() function.
• In higher level code related to handling of URLs of Django model instances: The get_absolute_url() method.
As mentioned, In order to perform URL reversing, you'll need to use named URL patterns. Choose names that are unlikely to clash and use prefixes to reduce the chances of this.
URL namespaces
Allow you to uniquely reverse URLs even if they share the same names. Furthermore they allow us to differentiate between multiple instances of an application.
urlpatterns = [ path(author-polls/, include(polls.urls, namespace=author-polls)), path(publisher-polls/, include(polls.urls, namespace=publisher-polls)), ]
These two paths share the same URL conf from the polls app, however author-polls:indexwill always resolve to the index page of the instance author-polls.
Django 2.2 -Models
Inherit from django.db.models.Model
Use python manage.py makemigrations and migrate commands to update database with models
Primary Key is a unique identifier for a row entry. A foreign key column links entries to matching rows in another table?
2.3
Models must be registered to view and manipulate in the admin page using admin.site.register(ModelName). Use python manage.py createsuperuser to create a user account for the admin page
2.4 Population scripts
It is a good idea to create scripts to populate your models with fake data ?for testing?.
pip install Faker
Using the faker library we can populate our models with data for the purposes of testing. Faker library is a quick and powerful tool.
The population script needs to start with the following import statements:
import os
os.environ.setdefault(‘DJANGO_SETTINGS_MODULE’, ‘your_project.settings’)
import django
django.setup()
import random
from your_app.models import your_models
from faker import Faker
2.5 MVT
The MVT paradigm follows these basic steps:
Import models into view
Use view to query models for required data
Pass data to template for rendering
Map a URL to the view
Using templates is demonstrated here; the for tag is used to loop through the given data object. An if Tag is used for flow control where data is not found.
LVL2 EXERCISE
3.1 Introduction. This section covers forms and processing user input
3.2 form concept walkthrough. Form creation is like model creation and even shares similar attributes. In the view, the form is passed as a context object to a template. The form is injected with template tagging with the following:
{{ form }} or {{ form.as_p }}
Use the {% csrf_token %} within forms to ensure security with django.
In the template the form is rendered with method=POST. The form VIEW will have a process where if request.method==‘POST’ then the process for validation, cleaning and saving form data is run.
3.3 forms code along
Create forms.py. Each class inherit from django.forms.Form.
Each form field has a name and a forms.FIELD which validates the form data to database scheme.
Views that process the form should assign the form function to a variable, and perform actions based on GET or POST requests (request.method).
For post requests, we use if form.is_valid() before processing the form data in any specific way. Validated data can be accessed using form.cleaned_data[‘form_attribute’]
3.4 Form validation.
Current form validation leaves form open to bot-filling. Add a hidden field with required = False as an attribute. We can catch bots by validating this input, where the field would be left empty by a human.
In the form, any function beginning with clean is tested by the form.valid option. A bot catcher would test self.cleaned_data[‘hidden_field’] and raid forms.ValidationError if it was greater than 0.
3.5 For,
