# Django development from scratch

*Nana Mensah, 01/11/17, v1*

This repo will document my process of learning **django web developement**. I aim to:
- describe the top-level concepts of django web development using multiple sources
- create a project to explore / practice new concepts
- create the STutors backend

## Information sources:
- [django user guide v1.11.6](https://docs.djangoproject.com/en/1.11/) - Definitive information on all concepts
- [django girls](https://tutorial.djangogirls.org/en/) - Guide for starting
- [tango with django v1.9](https://leanpub.com/tangowithdjango19/) - Guide for starting 2
- [mozilla django tutorial](https://developer.mozilla.org/en-US/docs/Learn/Server-side/Django) - Guide for beginners with detailed examples
- [pro django](http://www.apress.com/us/book/9781430258094) - Extensive top-level explanations of concepts, skim read for improving understanding

## Updates/Notes:

