# Import class with the required form fields
from django import forms
# Import User and Group model objects
from django.contrib.auth.models import User, Group
# Import User Creation Form
from django.contrib.auth.forms import UserCreationForm

class SmartrUserForm(UserCreationForm):
    pass
    '''A general form for creating new users on the smartr platform. Can be subclassed to add extra
    fields or user requirements.
    Note: Forms and ModelForms are designed to collect and validate user data from requests.
    This form presents the user with the fields: username, email, password. 
    Note: defining clean_<field_name>() will run this validation login when calling form.is_valid()
    Note: All form validation functions should raise ValidationError if there is an error. 
    '''
    # Create the group field. This form field is hidden and offers the choice of group using the
    # model fieldset. When the form is created, an 'initial' value for the group should be provided 
    # by the view.
    # user.group.add(Group.obects.get(name=user_group))
    # user.save

class SmartrLearnerForm(UserCreationForm):
     pass

class SmartrTutorForm(UserCreationForm):
    pass