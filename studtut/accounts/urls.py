# ===== Imports =====
# Url conf functions
from django.urls import path, include
# Views for use in URLconfs
from django.contrib.auth import views as auth_views
from django.views.generic.base import TemplateView
from . import views

urlpatterns = [
    # Render the Template for the index page, which links to the 'signup' and 'login' views.
    path('', TemplateView.as_view(template_name="accounts/index.html"), name='acc_index'),
    path('signup/<user_group>', views.signup, name='signup'),
    path('login/', auth_views.LoginView.as_view(), name='login'),
    path('prof_page/', views.profile_page, name='prof_page'),
    # url(r'profile/$', views.profile_page, name='profile')
]
