# Import django shortcuts for returning HTTP responses
from django.shortcuts import render, redirect
# Import the django 'reverse' function for calling urls by their URLconf name
from django.urls import reverse
# Import django user authentication functions
from django.contrib.auth import login, logout, authenticate
from django.contrib.auth.decorators import login_required
from django.contrib.auth.models import User, Group
from django.contrib.auth import views as auth_views
# Import custom form for creating user
from .forms import SmartrUserForm

def add_to_group(user, user_group):
    '''Function to add user to given group'''
    user.groups.add(Group.objects.get(name=user_group))
    user.save()

def signup(request, user_group):
    '''Process forms to create a new account using user information. Takes a user_group as an 
    argument to assign users to groupings.
    '''
    #TODO: WRITEUP out how forms/modelforms clean and validate data in django [from docs].
    # If the request method is GET, render the signup form with empty fields
    if request.method == 'GET':
        form = SmartrUserForm()
        return render(request, 'accounts/signup.html', {'form': form, 'user_group': user_group})
    # If the request method is POST, process the submitted user form
    elif request.method == 'POST':
        # Instantiate a UserCreationForm, populated with the POST data.
        form = SmartrUserForm(request.POST)
        # If the form data is valid, save user details, authenticate, and send user to profile page.
        if form.is_valid():
            # Save form data to the model (User)
            form.save()
            # ===== Instant Login =====
            username = form.cleaned_data.get('username')
            raw_pass = form.cleaned_data.get('password1') # One of two password validation fields
            user = authenticate(username=username, password=raw_pass)
            login(request, user)
            # ===== Add to appropriate group =====
            add_to_group(user,user_group)
            # Go to login page
            return redirect('https://www.google.com')
        # WHAT HAPPENS IF THE FORM IS INVALID? 
        # else:

@login_required()
def profile_page(request):
    return render(request, 'profiles/index.html')


def logout_view(request):
    logout(request)
    return redirect('index')
