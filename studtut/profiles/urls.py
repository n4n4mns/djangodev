''' urls for the profiles app.
After users have been signed in and authenticated through either the 'login' or 'signup' views of the
accounts app, they should be redirected to the files in this application.

Each user (learner or tutor) will be granted their own personal profile page.
'''
# ===== Imports =====
# URL conf functions
from django.urls import path, include
# Auth view for use in URLconfs
from django.contrib.auth import views as auth_views
# Generic views
from django.views.generic.base import TemplateView
# Personal views
from . import views

urlpatterns = [
    # url(r'login/$', auth_views.LoginView.as_view(), name='login'),
    # url(r'logout/$', views.logout_view, name='logout'),
    # url(r'profile/$', views.profile_page, name='profile')
]
