"""studtut URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/1.11/topics/http/urls/
"""

# ===== IMPORTS =====
# Import function for determining URLconfs.
from django.urls import include, path
# Import views for URLconf of admin site
from django.contrib import admin
# Import generic Redirect View to redirect homepage
from django.views.generic.base import RedirectView

urlpatterns = [
    # Path to admin site.
    path('admin/', admin.site.urls),
    # Path to redirect homepage. This view calls /accounts/, which displays a TemplateView for user auth.
    path('', RedirectView.as_view(url='accounts/'), name='index'),
    # Include the URLconf from the accounts app
    path('accounts/', include('accounts.urls'))
]
